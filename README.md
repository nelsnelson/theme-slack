# Slack application theme installation

<p align="center">
  <a>
    <img alt="Slack" title="Slack" src="slack.png" width="100">
  </a>
</p>

Install css theme for the Slack application.


## Usage

To install, invoke the following in a shell session of your choice.

```{sh}
git clone https://gitlab.com/nelsnelson/theme-slack.git
cd theme-slack
./install.sh
```


## Screenshot

![Screenshot](https://gitlab.com/nelsnelson/theme-slack/raw/master/screenshot.png "Screenshot")

©️ All rights reserved nels@nelsnelson.org


## License

[![GPLv3 License](https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat)][license]


## Donate

[![Patreon](https://img.shields.io/badge/patreon-donate-red.svg)][patreon]

[license]: https://gitlab.com/nelsnelson/theme-slack/blob/master/LICENSE
[patreon]: https://www.patreon.com/nelsnelson
