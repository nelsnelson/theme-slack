#! /usr/bin/env bash

# Usage: ./install.sh
#
# Install a Slack application theme.
#
# Restart Slack after installation.
#
# Adapted from: https://qiita.com/shoken/items/4f0bbba9b5d911657b5a
#

# Only requires root access if your Slack application has been installed 
# either System-wide on macOS (in /Applications), or another location 
# which your user does not have permission to modify.

THEME_FILE_NAME='night-dark.css'
#THEME_FILE_NAME='solarized-dark.css'

SCRIPT_NAME=$(basename "${0}")
PROJECT_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)"
UPDATE_ONLY=0
SLACK_THEME_SOURCE_PATH="${PROJECT_PATH}/${THEME_FILE_NAME}"
BACKUP=1
RESTORE=0
CLEANUP=1
DRYRUN=0
VERBOSE=0

usage() {
    echo "usage: $0 [-u] [-t <theme-css-file-path>] [-b] [-r] [-x] [-n] [-v]"
    echo "options:"
    echo " -u      Update Slack with an already installed css theme file"
    echo " -t      The theme file css to install; default: ${THEME_FILE_NAME}"
    echo " -b      Backup the Slack application"
    echo " -r      Restore the Slack application from a backup"
    echo " -x      Don't do any cleanup"
    echo " -n      Dry-run shows what would have been done"
    echo " -v      Verbose mode shows all commands executed"
    echo " -h, -?  Show this message"
}

while getopts "?hut:brxnv" opt; do
    case "$opt" in
    h)
        usage
        exit 0
        ;;
    u)  UPDATE_ONLY=1
        ;;
    t)  SLACK_THEME_SOURCE_PATH=$OPTARG
        ;;
    b)  BACKUP=1
        ;;
    r)  RESTORE=1
        ;;
    x)  CLEANUP=0
        ;;
    n)  DRYRUN=1
        ;;
    v)  VERBOSE=1
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

execute() {
    local COMMAND=$1
    if [ $DRYRUN -eq 1 ]
    then
        echo "Dry-run: ${COMMAND}"
    else
        if [ $VERBOSE -eq 1 ]
        then
            echo "Executing: ${COMMAND}"
        fi
        `${COMMAND}`
    fi
}

WORKSPACE='/tmp/slack'
EVENT_LISTENER_FILE_NAME='event-listener.js'
EVENT_LISTENER_PATH="${PROJECT_PATH}/${EVENT_LISTENER_FILE_NAME}"
SLACK_PATH_MACOS_LOCAL="${HOME}/Applications/Slack.app"
SLACK_PATH_MACOS_SYSTEM='/Applications/Slack.app'
SLACK_PATH_LINUX='/usr/lib/slack'
SLACK_USER_APPLICATION_SUPPORT="${HOME}/Library/Application\ Support/Slack"
SLACK_CONTAINERS_APPLICATION_SUPPORT="${HOME}/Library/Containers/com.tinyspeck.slackmacgap/Data/Library/Application\ Support/Slack"

if [[ -d $SLACK_PATH_MACOS_LOCAL ]]
then
    SLACK_PATH="${SLACK_PATH_MACOS_LOCAL}"
    SLACK_RESOURCES_PATH="${SLACK_PATH}/Contents/Resources"
elif [[ -d $SLACK_PATH_MACOS_SYSTEM ]]
then
    SLACK_PATH="${SLACK_PATH_MACOS_SYSTEM}"
    SLACK_RESOURCES_PATH="${SLACK_PATH}/Contents/Resources"
elif [[ -d $SLACK_PATH_LINUX ]]
then
    SLACK_PATH="${SLACK_PATH_LINUX}"
    SLACK_RESOURCES_PATH="${SLACK_PATH_LINUX}/resources"
else
    SLACK_PATH="${SLACK_PATH_MACOS_SYSTEM}"
    SLACK_RESOURCES_PATH="${SLACK_PATH}/Contents/Resources"
fi

# Fail early
if [[ ! -w "${SLACK_PATH}" ]]
then
    >&2 echo "Permission denied: ${SLACK_PATH}"
    >&2 echo "Please install as root: sudo ${0}"
    exit 1
fi

if [[ -f "${SLACK_USER_APPLICATION_SUPPORT}" ]]
then
    LOCAL_SETTINGS_PATH="${SLACK_USER_APPLICATION_SUPPORT}/local-settings.json"
elif [[ -f "${SLACK_CONTAINERS_APPLICATION_SUPPORT}" ]]
then
    LOCAL_SETTINGS_PATH="${SLACK_CONTAINERS_APPLICATION_SUPPORT}/local-settings.json"
else
    LOCAL_SETTINGS_PATH="${SLACK_USER_APPLICATION_SUPPORT}/local-settings.json"
fi

PACKED_APP_ASAR_PATH="${SLACK_RESOURCES_PATH}/app.asar"
UNPACKED_APP_ASAR_PATH="${SLACK_RESOURCES_PATH}/app.asar.unpacked"
SLACK_LEGACY_SSB_PATH="${UNPACKED_APP_ASAR_PATH}/src/static/ssb-interop.js"
SLACK_SSB_PATH="${UNPACKED_APP_ASAR_PATH}/dist/ssb-interop.bundle.js"

if [[ ! -z `which mdls` ]]
then
    SLACK_VERSION=`mdls -raw -name kMDItemVersion "${SLACK_PATH}"`
fi

# If requested, restore the Slack application from a backup, if it exists
if [[ $RESTORE -eq 1 ]]
then
    if [[ -d "${SLACK_PATH}.bak" ]]
    then
        execute "rm -rf ${SLACK_PATH}"
        execute "cp -an ${SLACK_PATH}.bak ${SLACK_PATH}"

        echo
        echo "Restored ${SLACK_PATH} from a backup"
        exit 0
    else
        >&2 echo
        >&2 echo "Cannot restore. No backup at: ${SLACK_PATH}.bak"
        exit 1
    fi
fi

if [[ ! -w "${LOCAL_SETTINGS_PATH}" ]]
then
    >&2 echo "Permission denied: ${LOCAL_SETTINGS_PATH}"
    >&2 echo "Please install as root: sudo ${0}"
    exit 1
fi

if [[ -z `which node` ]]; then
    >&2 echo "Install node.js: https://nodejs.org/en/"
    exit 1
fi

if [[ -z `which npm` ]]; then
    >&2 echo "Install npm: https://nodejs.org/en/"
    exit 1
fi

if [[ $VERBOSE -eq 1 ]]
then
    echo "Executing ${PROJECT_PATH}/${SCRIPT_NAME}"
fi

if [[ $DRYRUN -eq 1 ]]; then
    echo "Dry-run: sed -i 's/\"bootSonic\":\"once\"/\"bootSonic\":\"never\"/g' ${LOCAL_SETTINGS_PATH}"
else
    sed -i 's/"bootSonic":"once"/"bootSonic":"never"/g' ${LOCAL_SETTINGS_PATH}
fi

echo -n "Installing theme: ${SLACK_THEME_SOURCE_PATH} into ${SLACK_PATH}"
if [[ -z "${SLACK_VERSION}" ]]
then
    echo
else
    echo " version ${SLACK_VERSION}"
fi
echo

if [[ ! -f package-lock.json ]]
then
    # Initialize a node package so the asar node package
    # can be installed and used locally, instead of globally
    execute "npm init --yes"
fi

# Install the asar node package unless it is already installed
if [[ ! -x "${PROJECT_PATH}/node_modules/.bin/asar" ]]
then
    # Install the asar node package
    execute "npm install asar"
    if [ $? -ne 0 ]
    then
        >&2 echo
        >&2 echo "Failed to install required npm package: asar"
        >&2 echo "Aborting installation"
        exit 1
    fi
fi

if [[ -d "${WORKSPACE}" ]]
then
    # Clean up old workspaces
    execute "rm -rf ${WORKSPACE}"
fi

# Create a temporary workspace
execute "mkdir -p ${WORKSPACE}"

# Copy the theme css file to the Slack application resource directory
if [[ $DRYRUN -eq 1 ]]; then
    echo "Dry-run: cp -afv ${SLACK_THEME_SOURCE_PATH} ${SLACK_RESOURCES_PATH}/"
    SLACK_THEME_INSTALL_PATH="/DRYRUN/SLACK/THEME/INSTALL/PATH/PLACEHOLDER"
else
    SLACK_THEME_INSTALL_PATH=`cp -afv ${SLACK_THEME_SOURCE_PATH} ${SLACK_RESOURCES_PATH}/ | cut -d ' ' -f 3`

    echo "Installed theme to: ${SLACK_THEME_INSTALL_PATH}"
fi

# If the already unpacked app asar archive exists, put it in the workspace
if [[ -d "${UNPACKED_APP_ASAR_PATH}" ]]
then
    # Copy the original, unpacked asar archive directory to the
    # temporary workspace
    execute "cp -an ${UNPACKED_APP_ASAR_PATH} "${WORKSPACE}"/"
fi

# Copy the original, packed asar archive for Slack to the
# temporary workspace
execute "cp -an ${PACKED_APP_ASAR_PATH} ${WORKSPACE}/"

# Extract the asar archive
if [[ $DRYRUN -eq 1 ]]; then
    echo "Dry-run: pushd ${WORKSPACE}"
else
    pushd "${WORKSPACE}"
fi

execute "${PROJECT_PATH}/node_modules/.bin/asar extract ${WORKSPACE}/app.asar ${WORKSPACE}/app.asar.unpacked"

if [[ $? -eq 1 ]]
then
    >&2 echo
    >&2 echo "Unpacking the asar archive failed"
    >&2 echo "Aborting installation"
    exit 1
fi
if [[ $DRYRUN -eq 1 ]]; then
    echo "Dry-run: popd"
else
    popd
fi

# If the theme CSS file path is not already in the ssb-interop.bundle.js file,
# then append the event listener Javascript code to it, and insert the path
if [[ $DRYRUN -eq 1 ]]; then
    echo "Dry-run: grep -q \"${SLACK_THEME_INSTALL_PATH}\" ${WORKSPACE}/app.asar.unpacked/dist/ssb-interop.bundle.js"
else
    grep -q "${SLACK_THEME_INSTALL_PATH}" ${WORKSPACE}/app.asar.unpacked/dist/ssb-interop.bundle.js
fi

# If the SLACK_THEME_PATH is not in the ssb-interop.bundle.js file, then 
# the grep command result code will be 1
if [[ $? -eq 1 ]]
then
    # Add event listener Javascript code to Slack
    if [[ $DRYRUN -eq 1 ]]; then
        echo "Dry-run: cat ${EVENT_LISTENER_PATH} | tee -a ${WORKSPACE}/app.asar.unpacked/dist/ssb-interop.bundle.js"
    else
        cat ${EVENT_LISTENER_PATH} | tee -a ${WORKSPACE}/app.asar.unpacked/dist/ssb-interop.bundle.js
    fi

    # Insert the theme CSS file path in the event listener Javascript
    execute "sed -i -e s@SLACK_DARK_THEME_PATH@${SLACK_THEME_INSTALL_PATH}@g ${WORKSPACE}/app.asar.unpacked/dist/ssb-interop.bundle.js"
fi

# Re-pack up the asar archive
execute "${PROJECT_PATH}/node_modules/.bin/asar pack ${WORKSPACE}/app.asar.unpacked ${WORKSPACE}/app.asar"

# Backup the Slack application, if requested
if [[ $BACKUP -eq 1 ]]
then
    if [[ ! -d "${SLACK_PATH}.bak" ]]
    then
		execute "cp -an ${SLACK_PATH} ${SLACK_PATH}.bak"
	fi
fi

# Move the packed app.asar back into the Slack application
# resources directory
execute "cp ${WORKSPACE}/app.asar ${SLACK_RESOURCES_PATH}/app.asar"

if [[ $CLEANUP -eq 1 ]]
then
    # Clean up the workspace
    execute "rm -rf ${WORKSPACE}"
fi

if [[ $DRYRUN -eq 1 ]]
then
    echo
    echo "Dry-run complete"
    echo "Nothing was modified"
else
    echo
    echo "Theme installation is complete"
    echo "Restarting Slack"
    osascript -e 'quit app "Slack"'
    osascript -e 'run app "Slack"'
fi


